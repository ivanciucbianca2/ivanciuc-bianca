package edu.scoalainformala.week6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class ConverterTest {

    @Test
    void convertToMilimeter() {
            int value = 10;
            String unitateMasura = "cm";
            int rezultat = Converter.convertToMilimeter(value, unitateMasura);
            int rezultatAsteptat = 100; // 10 cm = 100 mm
            assertEquals(rezultatAsteptat, rezultat);
    }
    @Test
    void convertToCentimeters() {

        int value = 100;
        String unitateMasura = "mm";
        int rezultat = Converter.convertToCm(value, unitateMasura);
        int rezultatAsteptat = 10;
        assertEquals(rezultatAsteptat, rezultat);
    }
    @Test
    void testConvertToDm() {
        assertEquals(1, Converter.convertToDm(100, "mm"));
        assertEquals(5, Converter.convertToDm(50, "cm"));
        assertEquals(7, Converter.convertToDm(7, "dm"));
    }

    @Test
    void testConvertToM() {
        assertEquals(2, Converter.convertToM(2000, "mm"));
        assertEquals(3, Converter.convertToM(300, "cm"));
        assertEquals(5, Converter.convertToM(5, "m"));
    }

    @Test
    void testConvertToKm() {
        assertEquals(2, Converter.convertToKm(2000000, "mm"));
        assertEquals(4, Converter.convertToKm(4000, "m"));
        assertEquals(7, Converter.convertToKm(7, "km"));
    }
}
