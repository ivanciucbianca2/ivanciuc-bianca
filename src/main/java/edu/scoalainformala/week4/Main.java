package edu.scoalainformala.week4;

public class Main {

    public static void main(String[] args) {

        PhoneBase iPhone14 = new Iphone("S2489", "Black", "Silver", 100, "212");
        PhoneBase samsungGalaxy = new Samsung("S234", "Grey", "Platinum", 90, "333");

        iPhone14.addContact( "0749591349", "Bianca", "Ivanciuc");
        iPhone14.addContact( "0756265092", "Ana", "Timis");
        iPhone14.addContact( "0753265790", "Izabela", "Nemeth");
        iPhone14.addContact( "0746263091", "Daria", "Pop");

        Contact firstContact = iPhone14.getFirstContact();
        System.out.println(firstContact);

        Contact lastContact= iPhone14.getLastContact();

        System.out.println(lastContact);

        iPhone14.sendMessage("0749591349", "Hello, this is my channel, please enjoy it!");
        iPhone14.sendMessage("0749591349", "Did you get my message?");

        Message firstMessage = iPhone14.getFirstMessage("0749591349");
        Message secondMessage = iPhone14.getSecondMessage("0749591349");

        System.out.println(firstMessage);
        System.out.println(secondMessage);
        System.out.println();
        System.out.println();
        System.out.println("The" + firstContact + "is calling!");
        iPhone14.call("0753265790");
        System.out.println("After this call you have battery left: " + iPhone14.getBatteryLife());
        System.out.println();
        iPhone14.call("0746263091");
        iPhone14.viewHistory();

        System.out.println();
        System.out.println();
        samsungGalaxy.addContact( "0749591349", "Alex", "Ivanciuc");
        samsungGalaxy.addContact( "0756265092", "Andrei", "Timis");
        samsungGalaxy.addContact( "0753265790", "Ion", "Nemeth");
        samsungGalaxy.addContact( "0746263091", "Marius", "Pop");

        Contact primContact = samsungGalaxy.getFirstContact();
        System.out.println(primContact);

        Contact ultimateContact= samsungGalaxy.getLastContact();

        System.out.println(ultimateContact);
        samsungGalaxy.sendMessage("0756265092", "Hello, this is my channel, please enjoy it!");
        samsungGalaxy.sendMessage("0756265092", "Did you get my message?");
        System.out.println(firstMessage);
        System.out.println(secondMessage);
        System.out.println();
        System.out.println();
        System.out.println("The" + primContact + "is calling!");
        samsungGalaxy.call("0746263091");
        System.out.println("After this call you have battery left: " + samsungGalaxy.getBatteryLife());
        System.out.println();
        samsungGalaxy.call("0746263091");
        samsungGalaxy.viewHistory();
    }
}