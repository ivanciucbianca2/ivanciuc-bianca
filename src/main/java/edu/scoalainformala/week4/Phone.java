package edu.scoalainformala.week4;

public interface Phone {

    void addContact(String phoneNumber, String firstName, String lastName);

    void sendMessage(String phoneNumber, String messageContent);

    void callContact(String phoneNumber);

    void callContact(Contact contact);

    public void viewHistory();
}
