package edu.scoalainformala.week4;

import java.util.List;
import java.util.ArrayList;

public abstract class PhoneBase implements Phone {

    private String colorPhone;

    private String materialPhone;

    private int batteryLife;

    final String serialNumber;

    private List<Contact> contacts;

    private List<Message> messages;

    public List<String> getCallHistory() {
        return callHistory;
    }

    public void setCallHistory(List<String> callHistory) {
        this.callHistory = callHistory;
    }

    private List<String> callHistory;

    public PhoneBase(String colorPhone, String materialPhone, int batteryLife, String serialNumber) {
        this.colorPhone = colorPhone;
        this.materialPhone = materialPhone;
        this.batteryLife = batteryLife;
        this.serialNumber = serialNumber;
    }

    public PhoneBase(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getColorPhone() {
        return colorPhone;
    }

    public void setColorPhone(String colorPhone) {
        this.colorPhone = colorPhone;
    }

    public String getMaterialPhone() {
        return materialPhone;
    }

    public void setMaterialPhone(String materialPhone) {
        this.materialPhone = materialPhone;
    }

    public int getBatteryLife() {
        return batteryLife;
    }

    public void setBatteryLife(int batteryLife) {
        this.batteryLife = batteryLife;
    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    @Override
    public void addContact(String phoneNumber, String firstName, String lastName) {
        Contact contact = new Contact(phoneNumber, firstName, lastName);
        contacts.add(contact);
    }

    @Override
    public void sendMessage(String phoneNumber, String messageContent) {
        if (messageContent.length() > 500) {
            System.out.println("Message exceeds 500 characters limit");
            return;
        }
        batteryLife--;
        Message message = new Message(phoneNumber, messageContent);
        messages.add(message);
    }

    @Override
    public void callContact(String phoneNumber) {
        batteryLife -= 2;
    }

    @Override
    public void callContact(Contact contact) {
        String phoneNumber = contact.getPhoneNumber();

        batteryLife -= 2;

    }

    public Message getFirstMessage(String phoneNumber) {
        for (Message message : messages) {
            if (message.getPhoneNumber().equals(phoneNumber)) {
                return message;
            }
        }
        System.out.println("No messages found for phone number: " + phoneNumber);
        return null;
    }


    public Message getSecondMessage(String phoneNumber) {
        int count = 0;
        for (Message message : messages) {
            if (message.getPhoneNumber().equals(phoneNumber)) {
                count++;
                if (count == 2) {
                    return message;
                }
            }
        }
        System.out.println("No second message found for phone number: " + phoneNumber);
        return null;
    }


    public PhoneBase() {
        contacts = new ArrayList<>();
        serialNumber = null;
    }

    public Contact getFirstContact() {
        if (!contacts.isEmpty()) {
            return contacts.get(0);
        } else {
            System.out.println("No contact found!");
        }
        return null;
    }

    public Contact getLastContact() {
        if (!contacts.isEmpty()) {
            int lastIndex = contacts.size() - 1;
            return contacts.get(lastIndex);
        } else {
            System.out.println("No contact found!");
        }
        return null;
    }

    public void call(String phoneNumber) {
        callHistory.add("Called " + phoneNumber);
        batteryLife -= 2;
    }

    @Override
    public void viewHistory() {
        System.out.println(callHistory);
    }
}
