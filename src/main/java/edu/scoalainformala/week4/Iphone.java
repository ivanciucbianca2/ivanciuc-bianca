package edu.scoalainformala.week4;

import java.util.ArrayList;
import java.util.List;

public class Iphone extends PhoneBase {

    private String iOSVersion;

    public Iphone(String iOSVersion, String colorPhone, String materialPhone, int batteryLife, String serialNumber) {
        super(colorPhone, materialPhone, batteryLife, serialNumber);
        this.iOSVersion = iOSVersion;
        Contact contact1 = new Contact("Bianca", "Ivanciuc", "0745684454");
        Contact contact2 = new Contact("Diana", "Ivanciuc", "0745684454");
        Contact contact3 = new Contact("Gabrilea", "Ivanciuc", "0745684454");
        Contact contact4 = new Contact("Bianca", "Ivanciuc", "0745684454");
        List<Contact> contacts = new ArrayList<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        contacts.add(contact4);
        super.setContacts(contacts);
        super.setMessages(new ArrayList<>());
        super.setCallHistory(new ArrayList<>());
    }

    @Override
    public String toString() {
        return "Iphone{" +
                "iOSVersion='" + iOSVersion + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", contacts=" + getContacts() +
                '}';
    }
}
