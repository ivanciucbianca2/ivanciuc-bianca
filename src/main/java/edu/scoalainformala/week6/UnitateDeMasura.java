package edu.scoalainformala.week6;

public enum UnitateDeMasura {
    MM("mm"),
    CM("cm"),
    DM("dm"),
    M("m"),
    KM("km");

    private String name;

    UnitateDeMasura(String name) {
        this.name = name;
    }
}
