package edu.scoalainformala.week6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduceți un șir de caractere:");
        String expresie = scanner.nextLine();

        System.out.println("Introduceți unitatea de măsură pentru rezultat:");
        String unitateDeMasura = scanner.nextLine();

        Calculator calculator = new Calculator();
        calculator.setExpresie(expresie);
        Calculator.setUnitateMasura(unitateDeMasura);

        String rezultat = calculator.calculate();
        System.out.println("Rezultatul este: " + rezultat);

        scanner.close(); //
    }

}


