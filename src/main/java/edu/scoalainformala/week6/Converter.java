package edu.scoalainformala.week6;

import java.util.List;

public class Converter {

    public static int convertToMilimeter(int value, String unitateMasura) {
        switch (unitateMasura) {
            case "mm":
                return value;
            case "cm":
                return value * 10;
            case "dm":
                return value * 100;
            case "m":
                return value * 1000;
            case "km":
                return value * 1000000;
            default:
                System.out.println("Unitate de masura invalida");
                return 0;
        }
    }

    public static int convertToCm(int value, String unitateMasura) {
        switch (unitateMasura) {
            case "mm":
                return value / 10;
            case "cm":
                return value;
            case "dm":
                return value * 10;
            case "m":
                return value * 100;
            case "km":
                return value * 100000;
            default:
                System.out.println("Unitate de masura invalida");
                return 0;
        }
    }

    public static int convertToDm(int value, String unitateMasura) {
        switch (unitateMasura) {
            case "mm":
                return value / 100;
            case "cm":
                return value / 10;
            case "dm":
                return value;
            case "m":
                return value * 10;
            case "km":
                return value * 10000;
            default:
                System.out.println("Unitate de masura invalida");
                return 0;
        }
    }

    public static int convertToM(int value, String unitateMasura) {
        switch (unitateMasura) {
            case "mm":
                return value / 1000;
            case "cm":
                return value / 100;
            case "dm":
                return value / 10;
            case "m":
                return value;
            case "km":
                return value * 1000;
            default:
                System.out.println("Unitate de masura invalida");
                return 0;
        }
    }
    public static int convertToKm(int value, String unitateMasura) {
        switch (unitateMasura) {
            case "mm":
                return value / 1000000;
            case "cm":
                return value / 100000;
            case "dm":
                return value / 10000;
            case "m":
                return value / 1000;
            case "km":
                return value;
            default:
                System.out.println("Unitate de masura invalida");
                return 0;
        }
    }
}
